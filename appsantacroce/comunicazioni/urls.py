from django.urls import path
from . import views

urlpatterns = [
        path('comunicazione/', views.CreaComunicazione, name='Crea_Comunicazione'),
        path('comunicazione/lista_avvisi/', views.ListaComunicazioni.as_view(), name='Lista_Comunicazioni'),
        path('comunicazione/contatti', views.ContattiView, name="Lista_Contatti"),
        path('comunicazione/documento/', views.CaricaFile, name='Carica_File'),
        path('comunicazione/lista_documenti', views.DownloadFile.as_view(), name='Download_File'),
        path('comunicazione/lista_canti/', views.CantiList.as_view(), name='Lista_Canti'),
        path('comunicazione/file_canti_upload/', views.CaricaFileCanti, name='Carica_File_Canti'),
        path('comunicazione/canti_detail/<int:pk>', views.CantiDetailView.as_view(), name='Canti_Detail'),
        path('contatti', views.ContattiView, name='Contatti'),
        path('comunicazione/orari', views.OrariView, name='Orari'),
        path('comunicazione/cancella_avviso/<int:pk>', views.CancellaComunicazione.as_view(), name="Cancella_avviso"),
        path('comunicazioni/parrocchia', views.ParrocchiaView, name="Parrocchia"),
        path('comunicazioni/gruppi', views.GruppiViews, name="Gruppi"),
        path('comunicazioni/gallery', views.PhotoViews, name="Gallery")

]
