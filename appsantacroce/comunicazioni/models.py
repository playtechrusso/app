from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
import os

# Modello per i messaggi di avviso inseriti dall'amministratore
class Comunicazione(models.Model):
    titolo = models.CharField(max_length=80)
    contenuto = models.CharField(max_length=300)
    autore = models.ForeignKey(User, on_delete=models.CASCADE, related_name="comunicazioni_utente")
    data = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.titolo

    class Meta:
        verbose_name = "Comunicazione"
        verbose_name_plural= "Comunicazioni"


#Modello per il caricamento file
class Documento(models.Model):
    titolo = models.CharField(max_length=80)
    contenuto = models.CharField(max_length=300)
    data_caricamento = models.DateTimeField(auto_now=True)
    file = models.FileField(upload_to='uploads/')

    def __str__(self):
        return self.titolo

    def filename(self):
        return os.path.basename(self.file.name)

    class Meta:
        verbose_name = "Documento"
        verbose_name_plural = "Documenti"

#Modello per l 'inserimento di canti domenicali
class Canti(models.Model):
    titolo = models.CharField(max_length=90)
    contenuto = models.CharField(max_length=4000)
    data_caricamento = models.DateTimeField(auto_now=True)
    file_canti = models.FileField(upload_to='canti_uploads/', blank="True")

    def __str__(self):
        return self.titolo

    def filename(self):
        return os.path.basename(self.file_canti.name)

    def get_absolute_url(self):
        return reverse("Canti_Detail", kwargs={"pk" : self.pk})

    class Meta:
        verbose_name = "Canto"
        verbose_name_plural = "Canti"

#Modello per le immagini in galleria
class Photo(models.Model):
    photo = models.ImageField()
    data_caricamento = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.data_caricamento)

    def photoname(self):
        return "/media/"+os.path.basename(self.photo.name)
    # def get_absolute_url(self):
    #     return reverse()

    class Meta:
        verbose_name = "Foto"
        verbose_name_plural = "Foto"
