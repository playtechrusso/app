from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic.edit import CreateView, DeleteView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.core.paginator import Paginator
from .forms import ComunicazioneModelForm, DocumentoModelForm, FileCantiModelForm
from .models import Comunicazione, Documento, Canti, Photo
from .mixins import StaffMixing

# class Crea(StaffMixing, CreateView):
#     """ View to create a new comunication """
#     model = Comunicazione
#     fields = "__all__"
#     template_name = "comunicazioni/crea.html"
#     succes_url= "/"

class ListaComunicazioni(ListView):
    paginate_by = 5
    queryset = Comunicazione.objects.all().order_by("-data")
    template_name = 'comunicazioni/list_view.html'
    context_object_name = 'lista_comunicazioni'

@login_required
def CreaComunicazione(request):
    if request.method == "POST":
        form = ComunicazioneModelForm(request.POST)
        if form.is_valid():
            comunicazione = form.save(commit=False)
            comunicazione.autore = request.user
            comunicazione.save()
            return HttpResponseRedirect("/")
    else:
        form = ComunicazioneModelForm()
    context = {"form":form}
    return render(request, "comunicazioni/crea.html", context)

class CancellaComunicazione(DeleteView):
    model = Comunicazione
    success_url = "/"

def ContattiView(request):
    return render(request, "comunicazioni/contatti.html")

def OrariView(request):
    return render(request, "comunicazioni/orari.html")

@login_required
def CaricaFile(request):
    if request.method == "POST":
        form = DocumentoModelForm(request.POST, request.FILES)
        if form.is_valid():
            file = form.save(commit=False)
            file.save()
            return HttpResponseRedirect("/")
    else:
        form = DocumentoModelForm()
    context = {"form":form}
    return render(request, "comunicazioni/carica_documento.html", context)


class DownloadFile(ListView):
    paginate_by = 10
    model = Documento
    fields = ['file']
    template_name = 'comunicazioni/download.html'

@login_required
def CaricaFileCanti(request):
    if request.method == "POST":
        form = FileCantiModelForm(request.POST, request.FILES)
        print("Form received")
        if form.is_valid():
            file = form.save(commit=False)
            print("File Canti: ")

            file.save()
            return HttpResponseRedirect("/")
    else:
        form = FileCantiModelForm()
    context = {"form":form}
    return render(request, "comunicazioni/carica_file_canti.html", context)


class CantiList(ListView):
    paginate_by = 10
    model = Canti
    fields = ['file_canti']
    template_name = 'comunicazioni/file_canti.html'

class CantiDetailView(DetailView):
    model = Canti
    template_name = 'comunicazioni/canti_detail.html'

def ContattiView(request):
    return render(request, 'comunicazioni/contatti.html')

def ParrocchiaView(request):
    return render(request, 'comunicazioni/la_parrocchia.html')

def GruppiViews(request):
    return render(request, 'comunicazioni/gruppi_parrocchiali.html')

def PhotoViews(request):
    model = Photo.objects.all().order_by("-data_caricamento")
    context = {"Photo":model}
    return render(request, 'comunicazioni/foto.html', context)
