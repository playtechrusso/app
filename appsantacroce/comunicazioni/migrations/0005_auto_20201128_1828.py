# Generated by Django 3.1.2 on 2020-11-28 18:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('comunicazioni', '0004_auto_20201128_1810'),
    ]

    operations = [
        migrations.AlterField(
            model_name='photo',
            name='data_caricamento',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
