# Generated by Django 3.1.2 on 2020-11-02 18:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('comunicazioni', '0002_documento'),
    ]

    operations = [
        migrations.CreateModel(
            name='Canti',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titolo', models.CharField(max_length=90)),
                ('contenuto', models.CharField(max_length=4000)),
                ('data_caricamento', models.DateTimeField(auto_now=True)),
                ('file_canti', models.FileField(upload_to='canti_uploads/')),
            ],
            options={
                'verbose_name': 'Canto',
                'verbose_name_plural': 'Canti',
            },
        ),
    ]
