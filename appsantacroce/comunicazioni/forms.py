from django import forms

from .models import Comunicazione, Documento, Canti

class ComunicazioneModelForm(forms.ModelForm):
    contenuto = forms.CharField(widget= forms.Textarea(attrs={"rows":"5", "placeholder":"Scrivi una comunicazione.."}),
                max_length=4000, label="Comunicazione")
    class Meta:
        model = Comunicazione
        fields = ["titolo", "contenuto"]
        widgets = {
                "titolo":forms.TextInput(attrs={"placeholder":"Inserisci Titolo"})
        }

class DocumentoModelForm(forms.ModelForm):
    contenuto = forms.CharField(widget= forms.Textarea(attrs={"rows":"5", "placeholder":"Scrivi una descrizione.."}),
                max_length=4000, label="Descrizione file")

    class Meta:
        model = Documento
        fields = ["titolo", "contenuto", "file"]
        widgets = {
                "titolo":forms.TextInput(attrs={"placeholder":"Inserisci Titolo del file"})
                }


class FileCantiModelForm(forms.ModelForm):
    contenuto = forms.CharField(widget= forms.Textarea(attrs={"rows":"30", "placeholder":"Scrivi quì il testo.."}),
                max_length=4000, label="Testo")

    class Meta:
        model = Canti
        fields = ["titolo", "contenuto", "file_canti"]
        widgets = {
                "titolo":forms.TextInput(attrs={"placeholder":"Inserisci Titolo"})
                }
