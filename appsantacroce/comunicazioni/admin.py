from django.contrib import admin
from .models import Comunicazione, Documento, Photo, Canti
# Register your models here.

admin.site.register(Comunicazione)
admin.site.register(Documento)
admin.site.register(Canti)
admin.site.register(Photo)
