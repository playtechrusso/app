from django.contrib.auth.mixins import UserPassesTestMixin

class StaffMixing(UserPassesTestMixin):
    """
    Only the staff can create object
    """

    def test_func(self):
        return self.request.user.is_staff
