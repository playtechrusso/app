from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic.list import ListView
from comunicazioni.models import Comunicazione, Documento, Canti
from django.db.models.functions import Extract
import datetime

# Create your views here.
def homepage(request):
    ultima_comunicazione = Comunicazione.objects.last()
    if ultima_comunicazione is not None:
        data_oggi = datetime.datetime.now().date()
        differenza = data_oggi - ultima_comunicazione.data.date()
        numero_giorni = differenza.days
    else:
        numero_giorni = "empty"
    context = {"ultima_comunicazione":ultima_comunicazione, "numero_giorni":numero_giorni}
    return render(request, 'core/homepage.html', context)

def userProfileView(request, username):
    user = get_object_or_404(User, username=username)
    context = {"user":user}
    return render(request, 'core/user_profile.html', context)

class UserList(LoginRequiredMixin,ListView):
    model = User
    template_name = 'core/users.html'

def cerca(request):
    if "q" in request.GET:
        querystring = request.GET.get("q")
        if len(querystring) == 0:
            return redirect("/cerca/")
        comunicazioni = Comunicazione.objects.filter(titolo__icontains=querystring)
        documenti = Documento.objects.filter(contenuto__icontains=querystring)
        canti = Canti.objects.filter(titolo__icontains=querystring)
        context = {"comunicazioni":comunicazioni, "documenti":documenti, "canti":canti}
        return render(request, 'core/cerca.html', context)
    else:
        return render(request, 'core/cerca.html')
